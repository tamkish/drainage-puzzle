import { CSSProperties, FC, useState } from "react";
import { Button, Flex } from "antd";
import { ArrowDownOutlined } from "@ant-design/icons";

type ColumnId = "L" | "M" | "R";
type Color = CSSProperties["color"];
const definitions: {
  id: ColumnId;
  darkColor: Color;
  brightColor: Color;
  displayName: string;
  max: number;
  start: number;
  goal: number;
}[] = [
  {
    id: "L",
    darkColor: "#600",
    brightColor: "#f00",
    displayName: "red",
    max: 12,
    start: 12,
    goal: 6,
  },
  {
    id: "M",
    darkColor: "#060",
    brightColor: "#0f0",
    displayName: "green",
    max: 8,
    start: 0,
    goal: 6,
  },
  {
    id: "R",
    darkColor: "#006",
    brightColor: "#44f",
    displayName: "blue",
    max: 5,
    start: 0,
    goal: 0,
  },
];
const getDef = (id: ColumnId) =>
  definitions.find((def) => def.id == id) ?? definitions[0];

type Brick = {
  color: Color;
  isGoal: boolean;
  isWater: boolean;
  disabled: boolean;
};
const Brick: FC<Brick> = (props) => {
  return (
    <div
      style={{
        backgroundColor: props.isWater ? props.color : "#111",
        width: 100,
        height: 20,
        padding: "4px",
        margin: "5px",
        borderRadius: "5px",
      }}
    >
      {props.isGoal ? "*" : null}
    </div>
  );
};

type Column = {
  id: ColumnId;
  handler: (fr: ColumnId, to: ColumnId) => void;
  current: number;
  disabled: boolean;
};
const Column: FC<Column> = (props) => {
  return (
    <Flex vertical justify={"end"}>
      <div
        style={{
          border: `${getDef(props.id).brightColor} 2px dashed`,
          padding: "2px",
          margin: "10px",
        }}
      >
        {Array(getDef(props.id).max)
          .fill(null)
          .map((_, index) => {
            const isGoal =
              index >= getDef(props.id).max - getDef(props.id).goal;
            const isWater = index >= getDef(props.id).max - props.current;
            return (
              <Brick
                color={getDef(props.id).darkColor}
                key={index}
                isGoal={isGoal}
                isWater={isWater}
                disabled={props.disabled}
              />
            );
          })}
      </div>
      {definitions.map((target) => {
        if (target.id != props.id)
          return (
            <Button
              key={target.id}
              disabled={props.disabled}
              style={{
                backgroundColor: getDef(target.id).darkColor,
                color: "white",
              }}
              onClick={() => props.handler(props.id, target.id)}
            >
              <ArrowDownOutlined />
            </Button>
          );
        else return;
      })}
    </Flex>
  );
};

const GoalText: FC<{ id: ColumnId }> = ({ id }) => {
  const def = getDef(id);
  return (
    <span
      style={{
        color: def.brightColor,
        backgroundColor: def.darkColor,
      }}
    >
      {def.displayName}
    </span>
  );
};

export const App = () => {
  const [left, setLeft] = useState(getDef("L").start);
  const [mid, setMid] = useState(getDef("M").start);
  const [right, setRight] = useState(getDef("R").start);

  const values = {
    L: left,
    M: mid,
    R: right,
  };

  const [isMoving, setIsMoving] = useState(false);
  const [, /*isAllCorrect*/ setIsAllCorrect] = useState(false);

  const moveWater = (fr: ColumnId, to: ColumnId) => {
    if (isMoving) return;
    const valueFr = values[fr];
    const valueTo = values[to];
    const setFr = (() => {
      switch (fr) {
        case "L":
          return setLeft;
        case "M":
          return setMid;
        case "R":
          return setRight;
      }
    })();
    const setTo = (() => {
      switch (to) {
        case "L":
          return setLeft;
        case "M":
          return setMid;
        case "R":
          return setRight;
      }
    })();
    const maxTo = getDef(to).max;

    const needTo = maxTo - valueTo;
    const need = Math.min(needTo, valueFr);
    if (need <= 0) return;

    setIsMoving(true);
    const time = 50;
    for (let i = 1; i <= need; i++) {
      setTimeout(() => {
        setFr((prevState) => prevState - 1);
        setTo((prevState) => prevState + 1);

        if (i == need) {
          setIsMoving(false);
          if (definitions.every((def) => def.goal == values[def.id])) {
            setIsAllCorrect(true);
          }
        }
      }, i * time);
    }
  };

  return (
    <Flex vertical>
      <Flex>
        {definitions.map((definition) => (
          <Column
            id={definition.id}
            handler={moveWater}
            current={values[definition.id]}
            key={definition.id}
            disabled={/*isAllCorrect*/ false}
          />
        ))}
        {/*
      {isAllCorrect && <>you did it:)</>}
*/}
        {/*lazy to delete the isAllCorrect*/}
      </Flex>
      <div>
        Manipulate the liquid so that:
        <ul>
          <li>
            <GoalText id={"L"} /> <GoalText id={"M"} /> have equal height
            (marked by *)
          </li>
          <li>
            <GoalText id={"R"} /> is empty.
          </li>
        </ul>
        <br />
        There is no win screen.
        <br />
        Have fun.
      </div>
    </Flex>
  );
};
